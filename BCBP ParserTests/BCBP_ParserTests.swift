//
//  BCBP_ParserTests.swift
//  BCBP ParserTests
//
//  Created by Olcay Ertaş on 16.07.2018.
//  Copyright © 2018 Gözen Holding. All rights reserved.
//

import XCTest
@testable import BCBP_Parser

let barcodeSwiss01     = "M1DEMIRTAS/MUSTAFAMR  ER253PQ ISTZRHLX 1805 326Y017D0019 343>1011257242582520824                        E1130000000L"
let barcodeSwiss02     = "M1VANDENBERGH/PETERMR EKGVNWU ISTZRHLX 1805 326Y034D0089 343>1011252202567594052                        E1130020021E"
let barcodeSwiss03     = "M1RAWOG/DECHENCHOEDONMEQEVN4L ISTZRHLX 1805 326Y016B0066 343>1012257242128167193                        E1130010023K"

let barcodeUnited01    = "M1KURT/BAYRAMMR       EFEX2DE ISTEWRUA 0905 318Y028A 142 147]318  O2318BUA 401612280100129             0 UA"
let barcodeUnited02    = "M1DELEON/SALOME       EARTV0K ISTEWRUA 0905 032Y016E 199 15C]3180 O3032BUA              2901604613576051 UA                        *306      09"
let barcodeUnited03    = "M1COELHO/JOSEMR       ENGF7S0 ISTEWRUA 0905 032Y033K  81 15C]3180 O3031BUA 30161642200012901623524188111 UA LH 992001680838065     *30601    09"
let barcodeUnited04    = "M1RAMIREZ/INESM       EMJF0MJ ISTEWRUA 0905 318Y024E  97 147]318  O2318BUA              29             0 UA"
let barcodeUnited05    = "M1MCKNIGHT/SUSANBETH  EN4SN21 ISTEWRUA 0905 318Y031B  16 147]318  O2318BUA 401611242700129             0 UA UA KBB37036"
let barcodeUnited06    = "M1MCKNIGHT/DENNISWILLIEN4SN21 ISTEWRUA 0905 318Y031A  15 147]318  O2318BUA              29             1 UA UA PLS71165"
let barcodeUnited07    = "M1SANCHEZ/LIZAMARIE   EMJF0MJ ISTEWRUA 0905 318Y024D  95 147]318  O2318BUA              29             0 UA"
let barcodeUnited08    = "M1HILL/JEANHMRS       EH93CB8 ISTEWRUA 0905 318Y033D  56 147]318  O2318BUA 401612284200129             0 UA UA JSN02605"
let barcodeUnited09    = "M1SCHWARTZ/ANDREWMR   EGV182R ISTEWRUA 0905 318Y020D  62 147]318  O2318BUA              29             0 UA UA BG001065"
let barcodeUnited10    = "M1BARTIE/WILLIAMSTANLEEIK8SXF ISTEWRUA 0905 032Y022A  38 15C]3180 O3032BUA              2901671824778101 UA UA UDW95955            *306      09  UAS"

let barcodeDelta01     = "M1MARSHALL/CAROLANN   EGTAWVS ISTJFKDL 0073 228C02B 0016 10FDL000ZQOZKI02BN"
let barcodeDelta02     = "M1DEMIR/KRISTIN        GAGRA2 ISTJFKDL 0073 228C04C 0202 10FDL004LJRSGQ04CN"
let barcodeDelta03     = "M1KETCHERSIDE/ALISN    F7KIW8 ISTJFKDL 0073 228C05C 0200 10FDL004LDU61M05CN"
let barcodeDelta04     = "M1MANN/ANTHONYEDWARD  EHB28YC ISTJFKDL 0073 228C03B 0134 10FDL000XHUU1K03BN"
let barcodeDelta05     = "M1SAYGINAR/KUTLUHAN   EF984KK ISTJFKDL 0073 228C06F 0024 10FDL3JEGU44JV06FN"
let barcodeDelta06     = "M1FIELDER/KRISTIK     EF99NJA ISTJFKDL 0073 228C01E 0072 10FDL00307FETV01EN"
let barcodeDelta07     = "M1REED/JOHNMARTIN     EG7GNRU ISTJFKDL 0073 228C04B 0102 10FDL000AFGHD604BN"
let barcodeDelta08     = "M1NESTER/VIRGINIAEVELYEG7GNRU ISTJFKDL 0073 228C04A 0103 10FDL000XR7RKV04AN"
let barcodeDelta09     = "M1MANN/SALLYMINTZ     EHB28YC ISTJFKDL 0073 228C03A 0133 10FDL000XH2CM003AN"
let barcodeDelta10     = "M1GENOVESE/LINDALUELLAEGTAWVS ISTJFKDL 0073 228C02A 0015 10FDL000Z7Q7SI02AN"
let barcodeDelta11     = "M1KETCHERSIDE/ALEXVEHB F7KIW8 ISTJFKDL 0073 228C05E 0201 10FDL004LGO0PM05EN"
let barcodeDelta12     = "M1MATATOV/BORIS       EF92VLX ISTJFKDL 0073 228C01G 0090 10FDL3JEGZVKWO01GN"
let barcodeDelta13     = "M1WANG/JONATHAN       EF9A79J ISTJFKDL 0073 228C04E 0121 10FDL0012T97KA04EN"
let barcodeDelta14     = "M1MANN/ALEXANDRAROSE  EHB29GR ISTJFKDL 0073 228C05F 0140 10FDL0011DWJVX05FN"
let barcodeDelta15     = "M1MANN/ISABELLENICOLE EHB29GR ISTJFKDL 0073 228C05G 0141 10FDL0011DWK1705GN"
let barcodeDelta16     = "M1HOLT/MICHAELD        GAH39Y ISTJFKDL 0073 228C06E 0205 10FDL004MZ0YZK06EN"
let barcodeDelta17     = "M1FIKE/DAVIDJERRYMR   EF8MMTV ISTJFKDL 0073 228C06A 0159 10FDL0037DRPGY06AN"
let barcodeDelta18     = "M1SCHIRLING/ROBERT    EF73UJW ISTJFKDL 0073 228C02G 0114 10FDL001UC021X02GN"
let barcodeDelta19     = "M1MANN/GEORGIACARYN   EHB8B8W ISTJFKDL 0073 228C05B 0144 10FDL3JEKZ6EZD05BN"

let barcodeTHY01       = "M1GURSEL   ABDULLAH   ERTC35V ISTBHXTK 1967 277Y007D0139 150>10B1OO2277BTK 292352210880771 1                      020K                     X"
let barcodeTHY02       = "M1GAITANI  MARIA      ESRKH77 ISTLAXTK 0009 039C003K0021 150>10B2OO3039BTK 292352648354042 1                      020K                     X"
let barcodeTHY03       = "M1AREF     KAMARAN    ERKWTET ISTBHXTK 1967 277Y027D0116 150>10B1OO2277BTK 292352406305719 1                      020K                     X"
let barcodeTHY04       = "M1HENDERSON   MARTI   ES47UPR ISTBHXTK 1967 277Y023E0050 150>10B1OO2277BTK 292352267872060 1                      020K                     X"
let barcodeTHY05       = "M1HENDERSON   ANNE    ES47UPR ISTBHXTK 1967 277Y023F0051 150>10B2OO2277BTK 292352267872061 1                      020K                     X"
let barcodeTHY06       = "M1YURTSEVEN   ISMET   ETGJCNX ISTLAXTK 0009 039C002A0028 150>10B1OO3039BTK 292352407425902 1   TK ELITTK315752973 020K                     X"
let barcodeTHY07       = "M1MAAROOF  HISHO      ERKWTET ISTBHXTK 1967 277Y027E0117 150>10B2OO2277BTK 292352406305720 1                      020K                     X"

let barcodeLufthansa01 = "M1MATSUBARA/AKIHIROM  E5EHDXF ISTMUCLH 1773 326C002F0063 355>2180OO2326BLH              262202637523479  LH NH 4334917824       *30600000005"
let barcodeLufthansa02 = "M1KERSHAW/MARTINMR    E8QWZX8 ISTMUCLH 1773 326M036B0096 355>2180OO2326BLH              267242084300887  LH                     *30600000005"
let barcodeLufthansa03 = "M1ULLMANN/PIERREMR    E52XAXS ISTMUCLH 1773 326M019C0052 355>2180KO2326BLH              262202266892124  LH                     *30600000005"
let barcodeLufthansa04 = "M1JOINER/TERENCEMR    E8QWZX8 ISTMUCLH 1773 326M036A0095 355>2180OO2326BLH              267242084300886  LH                     *30600000005"
let barcodeLufthansa05 = "M1MLYNEK/JOSEFMR      E6SVM6H ISTMUCLH 1773 326M018F0044 355>2180KO2326BLH              262202581510764  LH LH 992225579347264 W*30600000005  LHS"
let barcodeLufthansa06 = "M1RAISBECK/ALISONMIS  E8QWZX8 ISTMUCLH 1773 326M036D0098 355>2180OO2326BLH              267242084300889  LH                     *30600000005"
let barcodeLufthansa07 = "M1RADDON/DENISEMISS   E8QWZX8 ISTMUCLH 1773 326M036C0097 355>2180OO2326BLH              267242084300888  LH                     *30600000005"
let barcodeLufthansa08 = "M1MAMPASI/LELOMRS     E6496DL ISTMUCLH 1773 326M033F0084 355>2180OO2326BLH 0220107934002262202327644699  LH                     *30602004105"

let barcodeIberia01    = "M1MOYA/RODRIGO        EJS05G  ISTMADIB 3313 326P022B0087 33D>10B0OO2326BIB 290752209710502 2"
let barcodeIberia02    = "M1BARRIENTOS/JUANCAR  EJJ4D6  ISTMADIB 3313 326N021D0061 33D>10B0OO2326BIB 290452209710514 2"

class BCBP_ParserTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testAll() {
        let codes = [
            barcodeSwiss01,
            barcodeSwiss02,
            barcodeSwiss03,
            barcodeUnited01,
            barcodeUnited02,
            barcodeUnited03,
            barcodeUnited04,
            barcodeUnited05,
            barcodeUnited06,
            barcodeUnited07,
            barcodeUnited08,
            barcodeUnited09,
            barcodeUnited10,
            barcodeDelta01,
            barcodeDelta02,
            barcodeDelta03,
            barcodeDelta04,
            barcodeDelta05,
            barcodeDelta06,
            barcodeDelta07,
            barcodeDelta08,
            barcodeDelta09,
            barcodeDelta10,
            barcodeDelta11,
            barcodeDelta12,
            barcodeDelta13,
            barcodeDelta14,
            barcodeDelta15,
            barcodeDelta16,
            barcodeDelta17,
            barcodeDelta18,
            barcodeDelta19,
            barcodeTHY01,
            barcodeTHY02,
            barcodeTHY03,
            barcodeTHY04,
            barcodeTHY05,
            barcodeTHY06,
            barcodeTHY07,
            barcodeLufthansa01,
            barcodeLufthansa02,
            barcodeLufthansa03,
            barcodeLufthansa04,
            barcodeLufthansa05,
            barcodeLufthansa06,
            barcodeLufthansa07,
            barcodeLufthansa08,
            barcodeIberia01,
            barcodeIberia02]
        
        for barcode in codes {
            testParse(barcode: barcode)
        }
    }
    
    func testParse(barcode: String) {
    
        let parser = BCBPParser(barcode: barcode)
        
        do {
            try parser.parse()
            guard let format = parser.format else {
                print("Format code is nil!")
                return
            }
            assert(parser.format == "M", "Format code should be M! Current format code is \(format)")
            let fullName = ((parser.name ?? "") + " " + (parser.surname ?? "")).padding(toLength: 21, withPad: " ", startingAt: 0)
            print("\((parser.title ?? "").padding(toLength: 4, withPad: " ", startingAt: 0)) " +
                    "\(fullName) " +
                    "\(parser.eTickedIndicator ?? "") " +
                    "\((parser.pnr ?? "").padding(toLength: 6, withPad: " ", startingAt: 0)) " +
                    "\(parser.fromCityAirportCode ?? "") " +
                    "\(parser.toCityAirportCode ?? "") \(parser.flightNumber ?? "") " +
                    "\(parser.flightDate ?? "") " +
                    "\(parser.compartment ?? "") " +
                    "\((parser.seatNumber ?? "").padding(toLength: 4, withPad: " ", startingAt: 0)) " +
                    "\((parser.checkinSequenceNumber ?? "").padding(toLength: 5, withPad: " ", startingAt: 0)) " +
                    "\(parser.passengerStatus ?? "")")
        } catch BCBPParseError.invalidLegCount {
            assertionFailure("Invalid leg count!")
        } catch BCBPParseError.invalidFormatCode {
            assertionFailure("Invalid format code!")
        } catch BCBPParseError.legCountParseError {
            assertionFailure("Invalid leg size!")
        } catch BCBPParseError.nameParseError {
            assertionFailure("Failed to parse name and surname!")
        } catch BCBPParseError.flightDateParseError {
            assertionFailure("Failed to parse flight date!")
        } catch {
            assertionFailure("Unknown exception!")
        }
    }
}
