//
//  BCBPParser.swift
//  GrabbaCatalog
//
//  Created by Olcay Ertaş on 12.07.2018.
//  Copyright © 2018 Grabba. All rights reserved.
//

import Foundation

@objc enum BCBPParseError: Int, Error {
    case invalidFormatCode
    case invalidLegCount
    case legCountParseError
    case nameParseError
    case flightDateParseError
}

/**
 A parser for Bar Coded Boarding Pass (BCBP) card codes.
 
 # The BCBP standard
 
 The Passenger Service Conference 2004, in unanimously approving Resolution 792,
 set BCBP as an industry standard. The IATA standard known as “Resolution 792
 Bar Coded Boarding Pass” defines “the required characteristics of the elements
 and format of the Bar Code on the Boarding Pass”. Its purpose is to provide
 solutions for an Electronic Ticket world and to fully replace the previous
 standard for boarding passes known as “Resolution 722c Automated
 Ticket/Boarding Pass – Version 2”. The Passenger Services Conference approved
 in 2008 a sunset date of 31 December 2010, which corresponded to the mandate
 for 100% BCBP set by IATA’s Board of Governors. It confirms that 100% BCBP
 corresponds to the elimination of mag stripes for boarding passes.
 
 The BCBP standard defines PDF417 as the unique symbology to be used to encode
 the data on paper boarding passes. PDF417 is an ISO standard bar code available
 in the public domain. It also defines that a mobile phone can be used as a
 boarding pass (mobile BCBP) provided that it can display one of three selected
 2D matrix codes.
 
 The BCBP standard enables the encoding up to 4 flight legs in the same BCBP.
 It must be noted that PDF417 is not scalable for large amount of data
 especially for a trip containing 4 leg segments with all conditional elements
 filled-in.
 
 ## Data format
 
 The BCBP standard enables airlines to encode either one flight leg or several
 legs into a single bar code and boarding pass. The format of the data in the
 2D bar code is defined in the BCBP standard.
 
 See iATA [documentation] [iATALink] for more detail.
 
 [iATALink]: https://www.iata.org/whatwedo/stb/Documents/BCBP-Implementation-Guide-5th-Edition-June-2016.pdf "iATA BCBP Document"
*/
@objc open class BCBPParser: NSObject {

    /// Scanned, unparsed barcode. A fixed-length field with 60 positions, according to the BCBP standard.
    @objc var barcode: String

    /// Format character. It should be M. Field length is 1.
    @objc var format: String?

    /// Number of flight legs encoded in barcode. Field length is 1.
    @objc var legCount: Int = 0

    @objc var title: String?

    @objc var name: String?

    @objc var surname: String?

    /// Name of the passenger. Field length is 20. If name is shorter than 20 it is filled with spaces.
    @objc var passengerName: String?

    /// Electronic ticket indicator. Field length is 1.
    @objc var eTickedIndicator: String?

    /// Operating carrier PNR code. Field length is 7.
    @objc var pnr: String?

    /// IATA 3-letter codes of departure airports. Field length is 3.
    @objc var fromCityAirportCode: String?

    /// IATA 3-letter codes of arrival airports. Field length is 3.
    @objc var toCityAirportCode: String?

    /// Codes of operating carrier. Field length is 3.
    @objc var operatingCarrierDesignator: String?

    /// Flight numbers. Field length of each flight number is 5.
    @objc var flightNumber: String?

    /// Flight date. Field length is 3.
    @objc var flightDate: String?

    /// Compartment code. Field length is 1.
    @objc var compartment: String?

    /// Seat number. Field length is 4.
    @objc var seatNumber: String?
    
    /// Checkin sequence number. Field length is 5.
    @objc var checkinSequenceNumber: String?
    
    /// Passenger status. Field length is 1.
    @objc var passengerStatus: String?

    /// Default init method.
    ///
    /// - Parameter barcode: Scanned, unparsed BCBP barcode. Fixed length is 60.
    init(barcode: String) {
        self.barcode = barcode
        super.init()
    }

    /// Parses barcode. If parsed barcode has error it is set to property error.
    /// Parsing stops when first error encountered.
    /// All the fields before first error position can be read.
    @objc func parse() throws {

        var index = 0

        for character in barcode {

            if index == 0 {

                if character != "M" {
                    throw BCBPParseError.invalidFormatCode
                }

                self.format = "\(character)"
            }

            if index == 1 {

                guard let legCount = Int(String(character)) else {
                    throw BCBPParseError.legCountParseError
                }

                if legCount < 1 || legCount > 4 {
                    throw BCBPParseError.invalidLegCount
                }

                self.legCount = legCount
            }

            if index == 2 {

                let startIndex = barcode.index(barcode.startIndex, offsetBy: 2)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 22)
                let nameSurnameAndTitle = barcode[startIndex..<endIndex].trimmingCharacters(in: .whitespacesAndNewlines)

                var components = nameSurnameAndTitle.components(separatedBy: [" ", "/"])

                components.removeAll { (item) -> Bool in
                    return item.isEmpty
                }

                if components.count == 2,
                   let first = components.first,
                   let last = components.last {

                    self.name = String(last)
                    self.surname = String(first)

                    if nameSurnameAndTitle.hasSuffix("MR") {
                        let startIndex = barcode.index(barcode.startIndex, offsetBy: nameSurnameAndTitle.count - 2)
                        self.title = String(nameSurnameAndTitle[startIndex...])
                        let toIndex = last.index(last.startIndex, offsetBy: last.count - 2)
                        self.name = String(last[..<toIndex])
                    }

                    if nameSurnameAndTitle.hasSuffix("MRS") {
                        let startIndex = barcode.index(barcode.startIndex, offsetBy: nameSurnameAndTitle.count - 3)
                        self.title = String(nameSurnameAndTitle[startIndex...])
                        let toIndex = last.index(last.startIndex, offsetBy: last.count - 3)
                        self.name = String(last[..<toIndex])
                    }

                    if nameSurnameAndTitle.hasSuffix("MISS") {
                        let startIndex = barcode.index(barcode.startIndex, offsetBy: nameSurnameAndTitle.count - 4)
                        self.title = String(nameSurnameAndTitle[startIndex...])
                        let toIndex = last.index(last.startIndex, offsetBy: last.count - 4)
                        self.name = String(last[..<toIndex])
                    }

                } else {
                    print(barcode)
                    print(components)
                    throw BCBPParseError.nameParseError
                }

                index = 22
                continue
            }

            if index == 22 {
                self.eTickedIndicator = String(character)
            }

            if index == 23 {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 23)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 29)
                let pnr = barcode[startIndex..<endIndex].trimmingCharacters(in: [" "])
                self.pnr = pnr.trimmingCharacters(in: [" "])
                index = 30
                continue
            }

            if (index == 30) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 30)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 33)
                self.fromCityAirportCode = String(barcode[startIndex..<endIndex])
                index = 33
            }

            if (index == 33) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 33)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 36)
                self.toCityAirportCode = String(barcode[startIndex..<endIndex])
                index = 36
                continue
            }

            if (index == 36) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 33)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 36)
                self.operatingCarrierDesignator = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
                index = 39
                continue
            }

            if (index == 39) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 39)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 44)
                self.flightNumber = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
                index = 44
                continue
            }

            if (index == 44) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 44)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 47)
                let jDays = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])

                guard let days = Int(jDays) else {
                    print("Failed to parse julian date")
                    throw BCBPParseError.flightDateParseError
                }

                let now = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy D"

                guard let date = dateFormatter.date(from: "\(Calendar.current.component(.year, from: now)) \(days)") else {
                    print("Failed to parse julian date")
                    throw BCBPParseError.flightDateParseError
                }

                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMMM"
                self.flightDate = formatter.string(from: date).padding(toLength: 12, withPad: " ", startingAt: 0) + String(Calendar.current.component(.year, from: date))
                index = 47
                continue
            }

            if (index == 47) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 47)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 48)
                self.compartment = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
            }

            if (index == 48) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 48)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 52)
                self.seatNumber = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
                index = 52
                continue
            }
            
            if (index == 52) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 52)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 57)
                self.checkinSequenceNumber = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
                index = 57
                continue
            }
            
            if (index == 57) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 57)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 58)
                self.passengerStatus = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
            }

            if (index == 58) {
                let startIndex = barcode.index(barcode.startIndex, offsetBy: 58)
                let endIndex = barcode.index(barcode.startIndex, offsetBy: 57)
                self.checkinSequenceNumber = String(barcode[startIndex..<endIndex]).trimmingCharacters(in: [" "])
                index = 57
                continue
            }

            index += 1
        }
    }
}
